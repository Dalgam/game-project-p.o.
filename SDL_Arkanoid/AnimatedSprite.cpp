#include "stdafx.h"
#include "AnimatedSprite.h"


//m_pxMyTempAnimatedSprite = m_xSystem.m_pxSpriteManager->CreateAnimatedSprite("yo"); <-get spritesheet
//m_pxMyTempAnimatedSprite->AddFrame(0, 0, 40, 16, 0.1f); <- first frame, do same for rest of frames

AnimatedSprite::AnimatedSprite(SDL_Texture* p_pxTexture) : Sprite(p_pxTexture)
{
	m_iIndex = 0;
	m_fCurrentDuration = 0.0f;
}

void AnimatedSprite::AddFrame(int p_iX, int p_iY, int p_iWidth, int p_iHeight, float p_fDuration)
{
	FrameData data;
	data.m_xRegion.x = p_iX;
	data.m_xRegion.y = p_iY;
	data.m_xRegion.w = p_iWidth;
	data.m_xRegion.h = p_iHeight;
	data.m_fDuration = p_fDuration;
	m_axFrames.push_back(data);
	m_xRegion.x = p_iX;
	m_xRegion.y = p_iY;
	m_xRegion.w = p_iWidth;
	m_xRegion.h = p_iHeight;

}


void AnimatedSprite::Update(float p_fDeltaTime, int p_iStartFrame, int p_iEndFrame)
{
	//TODO:All animated sprites need an extra frame at the end so that it doesn't read unassigned memory.Fix it if i got time

	m_fCurrentDuration += p_fDeltaTime;
	if (m_fCurrentDuration >= m_axFrames.at(m_iIndex).m_fDuration )
	{
		if (m_iIndex <= p_iEndFrame && m_iIndex >= p_iStartFrame)
		{
			
			m_xRegion = m_axFrames[m_iIndex].m_xRegion;
			m_iIndex++;
		}
		else
		{
			m_iIndex = p_iStartFrame;
			m_xRegion = m_axFrames[m_iIndex].m_xRegion;
		}
		
		m_fCurrentDuration = 0.0f;
	}
}
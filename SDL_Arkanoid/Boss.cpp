#include "stdafx.h"
#include "Boss.h"
#include "InputManager.h"
#include "AnimatedSprite.h"
#include "Collider.h"
#include "Projectile.h"


Boss::Boss(AnimatedSprite* p_pxAnimatedSprite, AnimatedSprite* p_pxProjectileAnimatedSprite,
	float p_fX, float p_fY, int p_iCameraWidth, int p_iCameraHeight, int p_iLevelWidth, int p_iLevelHeight)
{

	m_pxAnimatedSprite = p_pxAnimatedSprite;
	m_fX = p_fX;
	m_fY = p_fY;
	m_iCameraWidth = p_iCameraWidth;
	m_iCameraHeight = p_iCameraHeight;
	m_iLevelWidth = p_iLevelWidth;
	m_iLevelHeight = p_iLevelHeight;
	m_pxProjectileAnimatedSprite = p_pxProjectileAnimatedSprite;
	m_pxCollider = new Collider(m_pxAnimatedSprite->GetRegion()->w, m_pxAnimatedSprite->GetRegion()->h);
	m_pxCollider->SetParent(this);
	m_pxProjectileCenter = new Projectile(m_pxProjectileAnimatedSprite, m_fX, m_fY, 0, 0, m_iCameraWidth, m_iCameraHeight);
	m_pxProjectileTop = new Projectile(m_pxProjectileAnimatedSprite, m_fX, m_fY, 0, 0, m_iCameraWidth, m_iCameraHeight);
	m_pxProjectileBottom = new Projectile(m_pxProjectileAnimatedSprite, m_fX, m_fY, 0, 0, m_iCameraWidth, m_iCameraHeight);
	m_bVisible = true;
}
Boss::~Boss()
{
	delete m_pxProjectileBottom;
	m_pxProjectileBottom = nullptr;

	delete m_pxProjectileTop;
	m_pxProjectileTop = nullptr;

	delete m_pxProjectileCenter;
	m_pxProjectileCenter = nullptr;

	delete m_pxCollider;
	m_pxCollider = nullptr;

	
}
void Boss::Update(float p_fDeltaTime, int p_iCameraX, int p_iCameraY, float p_iPlayerPosX, float p_iPlayerPosY)
{

	m_pxAnimatedSprite->Update(p_fDeltaTime, 0, 1);

	if (m_iHealth <= 0)
	{
		m_bVisible = false;
	}
	

	m_pxProjectileCenter->Update(p_fDeltaTime, p_iCameraX, p_iCameraY);
	m_pxProjectileTop->Update(p_fDeltaTime, p_iCameraX, p_iCameraY);
	m_pxProjectileBottom->Update(p_fDeltaTime, p_iCameraX, p_iCameraY);
	m_fCurrentTime += p_fDeltaTime;
	if (m_fTimer <= m_fCurrentTime)
	{
		
		
		printf("projectile");
		m_pxProjectileCenter->SetPosition(m_fX, m_fY,-1.0f,0.0f);
		m_pxProjectileTop->SetPosition(m_fX, m_fY,-1.0f,-0.5f);
		m_pxProjectileBottom->SetPosition(m_fX, m_fY, -1.0f, 0.5f);
		m_fCurrentTime = 0.0f;
	}

	m_pxCollider->Refresh();
}

void Boss::SetPosition(float p_fX, float p_fY)
{
	m_fX = p_fX;
	m_fY = p_fY;
}

AnimatedSprite* Boss::GetProjectileAnimatedSprite() { return m_pxProjectileAnimatedSprite; }

AnimatedSprite* Boss::GetAnimatedSprite() { return m_pxAnimatedSprite; }

Collider* Boss::GetProjectileTopCollider()
{
	return m_pxProjectileTop->GetCollider();
}
Collider* Boss::GetProjectileCenterCollider()
{
  	return m_pxProjectileCenter->GetCollider();
}
Collider* Boss::GetProjectileBottomCollider()
{
	return m_pxProjectileBottom->GetCollider();
}

float Boss::GetProjectileTopX()
{
	return m_pxProjectileTop->GetX();
}
float Boss::GetProjectileTopY()
{
	return m_pxProjectileTop->GetY();
}
float Boss::GetProjectileCenterX()
{
	return m_pxProjectileCenter->GetX();
}
float Boss::GetProjectileCenterY()
{
	return m_pxProjectileCenter->GetY();
}
float Boss::GetProjectileBottomX()
{
	return m_pxProjectileBottom->GetX();
}
float Boss::GetProjectileBottomY()
{
	return m_pxProjectileBottom->GetY();
}
Collider* Boss::GetCollider() { return m_pxCollider; }

float Boss::GetX() { return m_fX; }

float Boss::GetY() { return m_fY; }

bool Boss::IsVisible() { return m_bVisible; }

EENTITYTYPE Boss::GetType() { return EENTITYTYPE::ENTITY_BOSS; }
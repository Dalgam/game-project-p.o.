#pragma once

#include "IEntity.h"

class AnimatedSprite;
class InputManager;
class Projectile;

class Boss : public IEntity
{
public:
	Boss(AnimatedSprite* p_pxAnimatedSprite, AnimatedSprite* p_pxProjectileAnimatedSprite, float p_fX, float p_fY, int p_iCameraWidth, int p_iCameraHeightint, int p_iLevelWidth, int p_iLevelHeight);
	~Boss();
	void Update(float p_fDeltaTime, int p_iCameraX, int p_iCameraY, float p_iPlayerPosX, float p_iPlayerPosY);
	AnimatedSprite* GetAnimatedSprite();
	AnimatedSprite* GetProjectileAnimatedSprite();
	Collider* GetProjectileTopCollider();
	Collider* GetProjectileCenterCollider();
	Collider* GetProjectileBottomCollider();
	float GetProjectileTopX();
	float GetProjectileTopY();
	float GetProjectileCenterX();
	float GetProjectileCenterY();
	float GetProjectileBottomX();
	float GetProjectileBottomY();
	Collider* GetCollider();
	float GetX();
	float GetY();
	int m_iHealth = 25;
	bool IsVisible();
	EENTITYTYPE GetType();
	void SetPosition(float p_fX, float p_fY);


private:
	Boss() {};
	AnimatedSprite* m_pxAnimatedSprite;
	AnimatedSprite* m_pxProjectileAnimatedSprite;
	Collider* m_pxCollider;
	InputManager* m_pxInputManager;
	Projectile* m_pxProjectileTop;
	Projectile* m_pxProjectileCenter;
	Projectile* m_pxProjectileBottom;
	float m_fX;
	float m_fY;
	float m_fCurrentTime = 5.0f;
	float m_fTimer = 1.5f;
	int m_iLevelWidth;
	int m_iLevelHeight;
	int m_iCameraWidth;
	int m_iCameraHeight;
	bool m_bVisible;


};
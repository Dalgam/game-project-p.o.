#pragma once

class Sprite;

class DrawManager
{
public:
	DrawManager();
	~DrawManager();


	/**
	* Initializes the DrawManager and creates an SDL_Renderer connected to an SDL_Window with the specified Width and Height defined in params.
	*/
	bool Initialize(int p_iLevelWidth, int p_iLevelHeight, int p_iCameraWidth, int p_iCameraHeight);
	void Shutdown();
	void Clear();
	void Present();
	int GetCameraX();
	int GetCameraY();
	void SetCameraX(int p_iNewX);
	void SetCameraY(int p_iNewY);
	void Draw(Sprite* p_pxSprite, int p_iX, int p_iY);

	SDL_Renderer* GetRenderer();
private:
	SDL_Rect m_xCamera;
	SDL_Window* m_pxWindow = nullptr;
	SDL_Renderer* m_pxRenderer = nullptr;
};

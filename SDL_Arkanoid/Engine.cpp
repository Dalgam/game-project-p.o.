#include "stdafx.h"
#include "Engine.h"
#include "InputManager.h"
#include "DrawManager.h"'
#include "SpriteManager.h"
#include "SoundManager.h"
#include "StateManager.h"
#include "Sprite.h"
#include "GameState.h"
#include "StartState.h"
#include "IState.h"
#include <iostream>
#include <ctime>

const int CAMERAWIDTH = 512;
const int CAMERAHEIGHT = 512;
const int LEVELHEIGHT = 2080;
const int LEVELWIDTH = 3040;
const int TILE_WIDTH = 32;
const int TILE_HEIGHT = 32;
const int TOTAL_TILES = 6175;
const int TOTAL_TILE_SPRITES = 176;

Engine::Engine()
{
	m_bRunning = false;
	m_pxDrawManager = nullptr;
	m_pxSpriteManager = nullptr;
	m_pxStateManager = nullptr;
	m_pxSoundManager = nullptr;

}

Engine::~Engine()
{

}


bool Engine::Initialize()
{
	// The initialize function will intialize libraries the program depends on and all manager we will create.

	srand((unsigned int)time(0)); //Initializes a random seed for when we will be using rand() in the future

	// Initializes the SDL library
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		return false;
	}
	int imgFlags = IMG_INIT_PNG;
	if (!(IMG_Init(imgFlags) & imgFlags))
	{
		printf (IMG_GetError());
		return false;
	}
	// Creates a new DrawManager and calls Initialize with width / height parameters.
	m_pxDrawManager = new DrawManager();
	if (m_pxDrawManager->Initialize(LEVELHEIGHT, LEVELHEIGHT,CAMERAWIDTH,CAMERAHEIGHT) == false)
	{
		return false;
	}


	m_pxSpriteManager = new SpriteManager(m_pxDrawManager->GetRenderer());

	m_pxStateManager = new StateManager();

	m_pxInputManager = new InputManager();
	m_pxSoundManager = new SoundManager();
	if (m_pxSoundManager->Initialize() == false)
	{
		return false;
	}
	
	System system;
	system.m_iLevelHeight = LEVELHEIGHT;
	system.m_iLevelWidth = LEVELWIDTH;
	system.m_iCameraWidth = CAMERAWIDTH;
	system.m_iCameraHeight = CAMERAHEIGHT;
	system.m_iTotalTiles = TOTAL_TILES;
	system.m_iTotalTileSprites = TOTAL_TILE_SPRITES;
	system.m_pxDrawManager = m_pxDrawManager;
	system.m_pxSpriteManager = m_pxSpriteManager;
	system.m_pxInputManager = m_pxInputManager;
	system.m_pxSoundManager = m_pxSoundManager;

	m_pxStateManager->SetState(new StartState(system));

	m_bRunning = true;

	return true;
}

void Engine::Shutdown()
{
	// The shutdown function will quit, delete and shutdown everything we have started up or created in initialize (In reverse order of creation)
	delete m_pxInputManager;
	m_pxInputManager = nullptr;
	
	delete m_pxStateManager;
	m_pxStateManager = nullptr;

	delete m_pxSpriteManager;
	m_pxSpriteManager = nullptr;

	m_pxSoundManager->Shutdown();
	delete m_pxSoundManager;
	m_pxSoundManager = nullptr;

	// Shuts down the drawmanager before deleting the object and nulling the pointer.
	m_pxDrawManager->Shutdown();
	delete m_pxDrawManager;
	m_pxDrawManager = nullptr;

	SDL_Quit();
}

void Engine::Update()
{
	// Our engines core loop
	while (m_bRunning)
	{
		m_pxInputManager->Update();
		m_pxDrawManager->Clear();
		if (m_pxStateManager->Update() == false)
		{
			m_bRunning = false;
		}
		m_pxStateManager->Draw();
		m_pxDrawManager->Present();
		//SDL_Delay(10);
	}
}
#pragma once

class DrawManager;
class SpriteManager;
class StateManager;
class InputManager;
class SoundManager;

class Engine
{
public:
	Engine();
	~Engine();

	/**
	* Initializes SDL and creates all Managers
	*/
	bool Initialize(); 
	
	/**
	* Quits SDL and deletes all Managers
	*/
	void Shutdown();

	/**
	* The Engines update loop
	*/
	void Update();


private:
	bool m_bRunning;
	DrawManager* m_pxDrawManager;
	SoundManager* m_pxSoundManager;
	SpriteManager* m_pxSpriteManager;
	StateManager* m_pxStateManager;
	InputManager* m_pxInputManager;
};

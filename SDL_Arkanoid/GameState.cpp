#include "stdafx.h"
#include <fstream>
#include <string>
#include "GameState.h"
#include "WinState.h"
#include "LoseState.h"
#include "SpriteManager.h"
#include "DrawManager.h"
#include "Sprite.h"
#include "Player.h"
#include "SwordVertical.h"
#include "SwordHorizontal.h"
#include "Boss.h"
#include "Tile.h"
#include "HUD.h"
#include "HUDElement.h"
#include "CollisionManager.h"
#include "AnimatedSprite.h"
#include "Sound.h"
#include "Music.h"
#include "SoundManager.h"

	

GameState::GameState(System& p_xSystem)
{
	
	m_xSystem = p_xSystem;
	m_pxFloor = nullptr;
	m_pxPlayer = nullptr;
	

}

GameState::~GameState()
{

}

void GameState::Enter()
{
	
	m_pxSlash = m_xSystem.m_pxSoundManager->CreateSound("../assets/Slash.wav");
	m_pxFanfare = m_xSystem.m_pxSoundManager->CreateSound("../assets/Fanfare.wav");
	m_pxMusic = m_xSystem.m_pxSoundManager->CreateMusic("../assets/Music.mp3");
	m_bWin = false;
	m_bPlayerInvincible = false;
	m_bBossInvincible = false;
	AnimatedSprite* xSpriteSwordVertical = m_xSystem.m_pxSpriteManager->CreateAnimatedSprite("../assets/LinkX2.png");
	xSpriteSwordVertical->AddFrame(10, 198, 14, 24, 0.075f);
	xSpriteSwordVertical->AddFrame(126, 168, 14, 24, 0.075f);
	xSpriteSwordVertical->AddFrame(126, 168, 14, 24, 0.075f);

	m_pxSwordVertical = new SwordVertical(m_xSystem.m_pxInputManager, xSpriteSwordVertical, 0, 0, m_xSystem.m_iCameraWidth, m_xSystem.m_iCameraHeight, m_xSystem.m_iLevelWidth, m_xSystem.m_iLevelHeight);
	
	AnimatedSprite* xSpriteSwordHorizontal = m_xSystem.m_pxSpriteManager->CreateAnimatedSprite("../assets/LinkX2.png");
	xSpriteSwordHorizontal->AddFrame(48, 190, 24, 14, 0.075f);
	xSpriteSwordHorizontal->AddFrame(198, 190, 24, 14, 0.075f);
	xSpriteSwordHorizontal->AddFrame(198, 190, 24, 14, 0.075f);

	m_pxSwordHorizontal = new SwordHorizontal(m_xSystem.m_pxInputManager, xSpriteSwordHorizontal, 0, 0, m_xSystem.m_iCameraWidth, m_xSystem.m_iCameraHeight, m_xSystem.m_iLevelWidth, m_xSystem.m_iLevelHeight);

	AnimatedSprite* xSpritePlayer = m_xSystem.m_pxSpriteManager->CreateAnimatedSprite("../assets/LinkX2.png");
	xSpritePlayer->AddFrame(0, 0, 32, 32, 0.075f);
	xSpritePlayer->AddFrame(0, 60, 32, 32, 0.075f);
	xSpritePlayer->AddFrame(60, 0, 32, 32, 0.075f);
	xSpritePlayer->AddFrame(60, 60, 32, 32, 0.075f);
	xSpritePlayer->AddFrame(120, 0, 32, 32, 0.075f);
	xSpritePlayer->AddFrame(120, 60, 32, 32, 0.075f);
	xSpritePlayer->AddFrame(180, 0, 32, 32, 0.075f);
	xSpritePlayer->AddFrame(180, 60, 32, 32, 0.075f);
	xSpritePlayer->AddFrame(180, 60, 32, 32, 0.075f);
	xSpritePlayer->AddFrame(0, 168, 32, 54, 0.075f);
	xSpritePlayer->AddFrame(48, 178, 54, 32, 0.075f);
	xSpritePlayer->AddFrame(120, 168, 32, 54, 0.075f);
	xSpritePlayer->AddFrame(168, 178, 54, 32, 0.075f);
	xSpritePlayer->AddFrame(168, 178, 54, 32, 0.075f);
	
	SDL_Rect* xRect = xSpritePlayer->GetRegion();

	m_pxPlayer = new Player(m_xSystem.m_pxInputManager,
		xSpritePlayer, m_pxSwordHorizontal, m_pxSwordVertical, 1824, 896,
		m_xSystem.m_iCameraWidth,
		m_xSystem.m_iCameraHeight, m_xSystem.m_iLevelWidth, m_xSystem.m_iCameraHeight);

	m_xSystem.m_pxDrawManager->SetCameraX(1536);
	m_xSystem.m_pxDrawManager->SetCameraY(544);

	
	AnimatedSprite* xSpriteHeart1 = m_xSystem.m_pxSpriteManager->CreateAnimatedSprite("../assets/Hearts.png");
	xSpriteHeart1->AddFrame(0, 0, 16, 16, 1.0f);
	xSpriteHeart1->AddFrame(16, 0, 16, 16, 1.0f);
	xSpriteHeart1->AddFrame(32, 0, 16, 16, 1.0f);
	xSpriteHeart1->AddFrame(32, 0, 16, 16, 1.0f);

	m_pxHeart1 = new HUDElement(xSpriteHeart1, m_xSystem.m_pxDrawManager->GetCameraX() + 390, m_xSystem.m_pxDrawManager->GetCameraY() + 112);

	AnimatedSprite* xSpriteHeart2 = m_xSystem.m_pxSpriteManager->CreateAnimatedSprite("../assets/Hearts.png");
	xSpriteHeart2->AddFrame(0, 0, 16, 16, 1.0f);
	xSpriteHeart2->AddFrame(16, 0, 16, 16, 1.0f);
	xSpriteHeart2->AddFrame(32, 0, 16, 16, 1.0f);
	xSpriteHeart2->AddFrame(32, 0, 16, 16, 1.0f);

	m_pxHeart2 = new HUDElement(xSpriteHeart2, m_xSystem.m_pxDrawManager->GetCameraX() + 406, m_xSystem.m_pxDrawManager->GetCameraY() + 112);

	AnimatedSprite* xSpriteHeart3 = m_xSystem.m_pxSpriteManager->CreateAnimatedSprite("../assets/Hearts.png");
	xSpriteHeart3->AddFrame(0, 0, 16, 16, 1.0f);
	xSpriteHeart3->AddFrame(16, 0, 16, 16, 1.0f);
	xSpriteHeart3->AddFrame(32, 0, 16, 16, 1.0f);
	xSpriteHeart3->AddFrame(32, 0, 16, 16, 1.0f);

	m_pxHeart3 = new HUDElement(xSpriteHeart3, m_xSystem.m_pxDrawManager->GetCameraX() + 422, m_xSystem.m_pxDrawManager->GetCameraY() + 112);

	AnimatedSprite* xSpriteBoss = m_xSystem.m_pxSpriteManager->CreateAnimatedSprite("../assets/Bosses.png");

	xSpriteBoss->AddFrame(8, 0, 48, 64, 0.075f);
	xSpriteBoss->AddFrame(98, 0, 48, 64, 0.075f);
	xSpriteBoss->AddFrame(188, 0, 48, 64, 0.075f);
	xSpriteBoss->AddFrame(278, 0, 48, 64, 0.075f);
	xSpriteBoss->AddFrame(278, 0, 48, 64, 0.075f);

	m_pxSpriteProjectile = m_xSystem.m_pxSpriteManager->CreateAnimatedSprite("../assets/Projectile.png");

	m_pxSpriteProjectile->AddFrame(0, 0, 8, 10, 1.0f);
	m_pxSpriteProjectile->AddFrame(16, 0, 8, 10, 1.0f);
	m_pxSpriteProjectile->AddFrame(32, 0, 8, 10, 1.0f);
	m_pxSpriteProjectile->AddFrame(48, 0, 8, 10, 1.0f);
	m_pxSpriteProjectile->AddFrame(80, 0, 8, 10, 1.0f);
	m_pxSpriteProjectile->AddFrame(96, 0, 8, 10, 1.0f);
	m_pxSpriteProjectile->AddFrame(112, 0, 8, 10, 1.0f);
	m_pxSpriteProjectile->AddFrame(128, 0, 8, 10, 1.0f);
	m_pxSpriteProjectile->AddFrame(144, 0, 8, 10, 1.0f);
	m_pxSpriteProjectile->AddFrame(160, 0, 8, 10, 1.0f);
	m_pxSpriteProjectile->AddFrame(176, 0, 8, 10, 1.0f);
	m_pxSpriteProjectile->AddFrame(192, 0, 8, 10, 1.0f);
	m_pxSpriteProjectile->AddFrame(208, 0, 8, 10, 1.0f);
	m_pxSpriteProjectile->AddFrame(224, 0, 8, 10, 1.0f);
	m_pxSpriteProjectile->AddFrame(240, 0, 8, 10, 1.0f);
	m_pxSpriteProjectile->AddFrame(240, 0, 8, 10, 1.0f);


	m_pxBoss = new Boss(xSpriteBoss, m_pxSpriteProjectile, 2368, 480, m_xSystem.m_iCameraWidth, m_xSystem.m_iCameraHeight, m_xSystem.m_iLevelWidth, m_xSystem.m_iLevelHeight);

	
	Sprite* xTriforce = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/Triforce1.png", 0, 0, 40, 40);
	TileData xTileDataTri;
	xTileDataTri.TileSprite = xTriforce;
	xTileDataTri.TileType = 1;

	m_pxTriforce = new Tile(xTileDataTri, 2796, 480);

	Sprite* xHUD = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/HUDDebug.png", 0, 0, 512, 512);
	xRect = xHUD->GetRegion();
	m_pxHUD = new HUD(xHUD, m_xSystem.m_pxDrawManager->GetCameraX(), m_xSystem.m_pxDrawManager->GetCameraY() - 352);



	SDL_Rect rect = { 0,0,32,32 };
	int xType;
	Sprite* xSprite = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/DungeonTS.png", 192, 192, rect.w, rect.h);
	TileData xTileData;
	xTileData.TileSprite = xSprite;
	xTileData.TileType = 2;
	m_axTileData.push_back(xTileData);
	for (int i = 0; i < 11; i++)
	{
		for (int j = 0; j < 16; j++)
		{
			rect.x = j * 32;

			Sprite* xSprite = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/DungeonTS.png", rect.x, rect.y, rect.w, rect.h);
			if (m_axTileData.size() > 0 && m_axTileData.size() < 37) { xType = 1; }
			else if (m_axTileData.size() > 142 && m_axTileData.size() < 177) { xType = 1; }
			else if (m_axTileData.size() > 43 && m_axTileData.size() < 53) { xType = 1; }
			else if (m_axTileData.size() > 53 && m_axTileData.size() < 56) { xType = 1; }
			else if (m_axTileData.size() > 61 && m_axTileData.size() < 69) { xType = 1; }
			else if (m_axTileData.size() > 76 && m_axTileData.size() < 83) { xType = 1; }
			else if (m_axTileData.size() > 94 && m_axTileData.size() < 101) { xType = 1; }
			else if (m_axTileData.size() > 108 && m_axTileData.size() < 117) { xType = 1; }
			else if (m_axTileData.size() > 126 && m_axTileData.size() < 133) { xType = 1; }
			else if (m_axTileData.size() > 142 && m_axTileData.size() < 177) { xType = 1; }
			else { xType = 2; }
			
			xTileData.TileSprite = xSprite;
			xTileData.TileType = xType;
			m_axTileData.push_back(xTileData);


		}
		rect.y += 32;
	}
	xSprite = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/DungeonTS.png", 192, 192, rect.w, rect.h);
	xTileData.TileSprite = xSprite;
	xTileData.TileType = 2;
	m_axTileData.push_back(xTileData);

	std::ifstream map("../assets/DungeonTileMap.map");
	int TileID;
	int TileX = 0;
	int TileY= 0;
	while(m_paxTiles.size() != m_xSystem.m_iTotalTiles)
	{
		int tileType = -1;
		map >> TileID;

		if ((TileID >= 0) && (TileID <= m_xSystem.m_iTotalTileSprites))
		{

			Tile* xTile = new Tile(m_axTileData[TileID], TileX, TileY);
			m_paxTiles.push_back(xTile);
		}
		TileX += 32;
		if (TileX  >= m_xSystem.m_iLevelWidth)
		{
			TileX = 0;
			TileY += 32;
		}
		
	}
	m_pxMusic->PlayMusic(-1);
}

void GameState::Exit()
{
	auto it = m_paxTiles.begin();
	while (it != m_paxTiles.end())
	{
		m_xSystem.m_pxSpriteManager->DestroySprite((*it)->GetSprite());
		delete (*it);
		it++;
	}
	m_paxTiles.clear();

	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxHUD->GetSprite());
	delete m_pxHUD;
	m_pxHUD = nullptr;

	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxTriforce->GetSprite());
	delete m_pxTriforce;
	m_pxTriforce = nullptr;

	if (m_pxBoss != nullptr)
	{
		m_xSystem.m_pxSpriteManager->DestroySprite(m_pxBoss->GetAnimatedSprite());
		delete m_pxBoss;
		m_pxBoss = nullptr;
	}

	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxHeart3->GetAnimatedSprite());
	delete m_pxHeart3;
	m_pxHeart3 = nullptr;

	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxHeart2->GetAnimatedSprite());
	delete m_pxHeart2;
	m_pxHeart2 = nullptr;

	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxHeart1->GetAnimatedSprite());
	delete m_pxHeart1;
	m_pxHeart1 = nullptr;

	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxPlayer->GetAnimatedSprite());
	delete m_pxPlayer;
	m_pxPlayer = nullptr;

	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxSwordHorizontal->GetAnimatedSprite());
	delete m_pxSwordHorizontal;
	m_pxSwordHorizontal = nullptr;

	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxSwordVertical->GetAnimatedSprite());
	delete m_pxSwordVertical;
	m_pxSwordVertical = nullptr;
	
	


}

bool GameState::Update(float p_fDeltaTime)
{
	m_pxBoss->GetProjectileAnimatedSprite()->Update(p_fDeltaTime,0,14);
	m_pxPlayer->Update(p_fDeltaTime, m_xSystem.m_pxDrawManager->GetCameraX(), m_xSystem.m_pxDrawManager->GetCameraY());
	
	if (m_pxPlayer->GetX() + 32 == m_xSystem.m_iCameraWidth + m_xSystem.m_pxDrawManager->GetCameraX())
	{
		UpdateCamera(RIGHT);
	}
	if (m_pxPlayer->GetX() == m_xSystem.m_pxDrawManager->GetCameraX())
	{
		UpdateCamera(LEFT);
	}
	if (m_pxPlayer->GetY() + 32 == m_xSystem.m_iCameraHeight + m_xSystem.m_pxDrawManager->GetCameraY())
	{
		UpdateCamera(DOWN);
	}
	if (m_pxPlayer->GetY() == 160 + m_xSystem.m_pxDrawManager->GetCameraY())
	{
		UpdateCamera(UP);
	}

	//Update Hearts or ends the game
	if (m_pxPlayer->m_iHealth == 0 || m_bWin)
	{
		return false;
	}
	else if (m_pxPlayer->m_iHealth == 1)
	{
		m_pxHeart1->GetAnimatedSprite()->Update(p_fDeltaTime, 1, 1);
		m_pxHeart2->GetAnimatedSprite()->Update(p_fDeltaTime, 2, 2);
		m_pxHeart3->GetAnimatedSprite()->Update(p_fDeltaTime, 2, 2);
	}
	else if (m_pxPlayer->m_iHealth == 2)
	{
		m_pxHeart1->GetAnimatedSprite()->Update(p_fDeltaTime, 0, 0);
		m_pxHeart2->GetAnimatedSprite()->Update(p_fDeltaTime, 2, 2);
		m_pxHeart3->GetAnimatedSprite()->Update(p_fDeltaTime, 2, 2);
	}
	else if (m_pxPlayer->m_iHealth == 3)
	{
		m_pxHeart1->GetAnimatedSprite()->Update(p_fDeltaTime, 0, 0);
		m_pxHeart2->GetAnimatedSprite()->Update(p_fDeltaTime, 1, 1);
		m_pxHeart3->GetAnimatedSprite()->Update(p_fDeltaTime, 2, 2);
	}
	else if (m_pxPlayer->m_iHealth == 4)
	{
		m_pxHeart1->GetAnimatedSprite()->Update(p_fDeltaTime, 0, 0);
		m_pxHeart2->GetAnimatedSprite()->Update(p_fDeltaTime, 0, 0);
		m_pxHeart3->GetAnimatedSprite()->Update(p_fDeltaTime, 2, 2);
	}
	else if (m_pxPlayer->m_iHealth == 5)
	{
		m_pxHeart1->GetAnimatedSprite()->Update(p_fDeltaTime, 0, 0);
		m_pxHeart2->GetAnimatedSprite()->Update(p_fDeltaTime, 0, 0);
		m_pxHeart3->GetAnimatedSprite()->Update(p_fDeltaTime, 1, 1);
	}
	else if (m_pxPlayer->m_iHealth == 6)
	{
		m_pxHeart1->GetAnimatedSprite()->Update(p_fDeltaTime, 0, 0);
		m_pxHeart2->GetAnimatedSprite()->Update(p_fDeltaTime, 0, 0);
		m_pxHeart3->GetAnimatedSprite()->Update(p_fDeltaTime, 0, 0);
	}
	
	
	m_pxSpriteProjectile->Update(p_fDeltaTime, 0, 13);

	m_pxSwordHorizontal->Update(p_fDeltaTime, m_xSystem.m_pxDrawManager->GetCameraX(), m_xSystem.m_pxDrawManager->GetCameraY());
	m_pxSwordVertical->Update(p_fDeltaTime, m_xSystem.m_pxDrawManager->GetCameraX(), m_xSystem.m_pxDrawManager->GetCameraY());
	
	if (m_pxBoss->GetX() > m_xSystem.m_pxDrawManager->GetCameraX() && m_pxBoss->GetY() > m_xSystem.m_pxDrawManager->GetCameraY() && m_pxBoss->IsVisible())
	{
		if (m_pxBoss->GetX() < m_xSystem.m_pxDrawManager->GetCameraX() + 512 && m_pxBoss->GetY() < m_xSystem.m_pxDrawManager->GetCameraY() + 512)
		{
			m_pxBoss->Update(p_fDeltaTime, m_xSystem.m_pxDrawManager->GetCameraX(), m_xSystem.m_pxDrawManager->GetCameraY(), m_pxPlayer->GetX(), m_pxPlayer->GetY());
		}
	}
	

	CheckCollision();
	return true;
}

void GameState::UpdateCamera(int p_iDirection)
{
	int xOldPositionX = m_xSystem.m_pxDrawManager->GetCameraX();
	int xOldPositionY = m_xSystem.m_pxDrawManager->GetCameraY();
	if (p_iDirection == RIGHT)
	{
		while (m_xSystem.m_pxDrawManager->GetCameraX() != xOldPositionX + 512)
		{
			
			m_xSystem.m_pxDrawManager->SetCameraX(m_xSystem.m_pxDrawManager->GetCameraX() + 4);
			m_pxPlayer->SetPosition(m_pxPlayer->GetX() + 0.5f, m_pxPlayer->GetY());
			m_pxHUD->SetPosition(m_xSystem.m_pxDrawManager->GetCameraX(), m_xSystem.m_pxDrawManager->GetCameraY() - 352);
			m_pxHeart1->SetPosition(m_pxHeart1->GetX() + 4, m_pxHeart1->GetY());
			m_pxHeart2->SetPosition(m_pxHeart2->GetX() + 4, m_pxHeart2->GetY());
			m_pxHeart3->SetPosition(m_pxHeart3->GetX() + 4, m_pxHeart3->GetY());
			m_xSystem.m_pxDrawManager->Clear();
			Draw();
			m_xSystem.m_pxDrawManager->Present();
		}
	}
	if (p_iDirection == LEFT)
	{
		while (m_xSystem.m_pxDrawManager->GetCameraX() != xOldPositionX - 512)
		{
			
			m_xSystem.m_pxDrawManager->SetCameraX(m_xSystem.m_pxDrawManager->GetCameraX() - 4);
			m_pxPlayer->SetPosition(m_pxPlayer->GetX() - 0.5f, m_pxPlayer->GetY());
			m_pxHUD->SetPosition(m_xSystem.m_pxDrawManager->GetCameraX(), m_xSystem.m_pxDrawManager->GetCameraY() - 352);
			m_pxHeart1->SetPosition(m_pxHeart1->GetX() - 4,m_pxHeart1->GetY());
			m_pxHeart2->SetPosition(m_pxHeart2->GetX() - 4, m_pxHeart2->GetY());
			m_pxHeart3->SetPosition(m_pxHeart3->GetX() - 4, m_pxHeart3->GetY());
			m_xSystem.m_pxDrawManager->Clear();
			Draw();
			m_xSystem.m_pxDrawManager->Present();
		}
	}
	if (p_iDirection == DOWN)
	{
		while (m_xSystem.m_pxDrawManager->GetCameraY() != xOldPositionY + 352)
		{
			
			m_xSystem.m_pxDrawManager->SetCameraY(m_xSystem.m_pxDrawManager->GetCameraY() + 4);
			m_pxPlayer->SetPosition(m_pxPlayer->GetX() , m_pxPlayer->GetY() + 0.5f);
			m_pxHUD->SetPosition(m_xSystem.m_pxDrawManager->GetCameraX(), m_xSystem.m_pxDrawManager->GetCameraY() - 352);
			m_pxHeart1->SetPosition(m_pxHeart1->GetX(), m_pxHeart1->GetY() + 4);
			m_pxHeart2->SetPosition(m_pxHeart2->GetX(), m_pxHeart2->GetY() + 4);
			m_pxHeart3->SetPosition(m_pxHeart3->GetX(), m_pxHeart3->GetY() + 4);
			m_xSystem.m_pxDrawManager->Clear();
			Draw();
			m_xSystem.m_pxDrawManager->Present();
		}
	}
	if (p_iDirection == UP)
	{
		while (m_xSystem.m_pxDrawManager->GetCameraY() != xOldPositionY - 352)
		{
			
			m_xSystem.m_pxDrawManager->SetCameraY(m_xSystem.m_pxDrawManager->GetCameraY() - 4);
			m_pxPlayer->SetPosition(m_pxPlayer->GetX() , m_pxPlayer->GetY() - 0.5f);
			m_pxHUD->SetPosition(m_xSystem.m_pxDrawManager->GetCameraX(), m_xSystem.m_pxDrawManager->GetCameraY() - 352);
			m_pxHeart1->SetPosition(m_pxHeart1->GetX(), m_pxHeart1->GetY() - 4);
			m_pxHeart2->SetPosition(m_pxHeart2->GetX(), m_pxHeart2->GetY() - 4);
			m_pxHeart3->SetPosition(m_pxHeart3->GetX(), m_pxHeart3->GetY() - 4);
			m_xSystem.m_pxDrawManager->Clear();
			Draw();
			m_xSystem.m_pxDrawManager->Present();
		}
	}

}

void GameState::Draw()
{
	
	m_xSystem.m_pxDrawManager->Draw(m_pxHUD->GetSprite(), m_xSystem.m_pxDrawManager->GetCameraX(),  m_xSystem.m_pxDrawManager->GetCameraY());
	
	
	auto it = m_paxTiles.begin();
	while (it != m_paxTiles.end())
	{
		if ((*it)->GetX() > m_xSystem.m_pxDrawManager->GetCameraX() - 32 && (*it)->GetY() > m_xSystem.m_pxDrawManager->GetCameraY() + 28)
		{
			if ((*it)->GetX() < m_xSystem.m_pxDrawManager->GetCameraX() + 512 && (*it)->GetY() < m_xSystem.m_pxDrawManager->GetCameraY() + 512)
			{
			m_xSystem.m_pxDrawManager->Draw(
					(*it)->GetSprite(),
					(*it)->GetX() - m_xSystem.m_pxDrawManager->GetCameraX(),
					(*it)->GetY() - m_xSystem.m_pxDrawManager->GetCameraY()
					);
			}
		}
		it++;
	}

	if (m_pxBoss->GetX() > m_xSystem.m_pxDrawManager->GetCameraX() && m_pxBoss->GetY() > m_xSystem.m_pxDrawManager->GetCameraY() && m_pxBoss->IsVisible())
	{
		if (m_pxBoss->GetX() < m_xSystem.m_pxDrawManager->GetCameraX() + 512 && m_pxBoss->GetY() < m_xSystem.m_pxDrawManager->GetCameraY() + 512)
		{
			m_xSystem.m_pxDrawManager->Draw(m_pxBoss->GetAnimatedSprite(), m_pxBoss->GetX() - m_xSystem.m_pxDrawManager->GetCameraX(), m_pxBoss->GetY() - m_xSystem.m_pxDrawManager->GetCameraY());
			m_xSystem.m_pxDrawManager->Draw(m_pxSpriteProjectile, m_pxBoss->GetProjectileBottomX() - m_xSystem.m_pxDrawManager->GetCameraX() , m_pxBoss->GetProjectileBottomY() - m_xSystem.m_pxDrawManager->GetCameraY());
			m_xSystem.m_pxDrawManager->Draw(m_pxSpriteProjectile, m_pxBoss->GetProjectileCenterX() - m_xSystem.m_pxDrawManager->GetCameraX(), m_pxBoss->GetProjectileCenterY() - m_xSystem.m_pxDrawManager->GetCameraY());
			m_xSystem.m_pxDrawManager->Draw(m_pxSpriteProjectile, m_pxBoss->GetProjectileTopX() - m_xSystem.m_pxDrawManager->GetCameraX(), m_pxBoss->GetProjectileTopY() - m_xSystem.m_pxDrawManager->GetCameraY());
		}
	}
	m_xSystem.m_pxDrawManager->Draw(m_pxTriforce->GetSprite(),  2796 - m_xSystem.m_pxDrawManager->GetCameraX(),  480 - m_xSystem.m_pxDrawManager->GetCameraY());

	m_xSystem.m_pxDrawManager->Draw(m_pxHUD->GetSprite(),m_pxHUD->GetX() - m_xSystem.m_pxDrawManager->GetCameraX(),m_pxHUD->GetY() - m_xSystem.m_pxDrawManager->GetCameraY());
	
	m_xSystem.m_pxDrawManager->Draw(m_pxHeart1->GetAnimatedSprite() , m_pxHeart1->GetX() - m_xSystem.m_pxDrawManager->GetCameraX(), m_pxHeart1->GetY() - m_xSystem.m_pxDrawManager->GetCameraY());
	m_xSystem.m_pxDrawManager->Draw(m_pxHeart2->GetAnimatedSprite(), m_pxHeart2->GetX() - m_xSystem.m_pxDrawManager->GetCameraX(), m_pxHeart2->GetY() - m_xSystem.m_pxDrawManager->GetCameraY());
	m_xSystem.m_pxDrawManager->Draw(m_pxHeart3->GetAnimatedSprite(), m_pxHeart3->GetX() - m_xSystem.m_pxDrawManager->GetCameraX(), m_pxHeart3->GetY() - m_xSystem.m_pxDrawManager->GetCameraY());
	
	
	m_xSystem.m_pxDrawManager->Draw(m_pxPlayer->GetAnimatedSprite(), m_pxPlayer->GetX() - m_xSystem.m_pxDrawManager->GetCameraX(), m_pxPlayer->GetY() - m_xSystem.m_pxDrawManager->GetCameraY());

	m_xSystem.m_pxDrawManager->Draw(m_pxSwordHorizontal->GetAnimatedSprite(), m_pxSwordHorizontal->GetX() - m_xSystem.m_pxDrawManager->GetCameraX(), m_pxSwordHorizontal->GetY() - m_xSystem.m_pxDrawManager->GetCameraY());
	m_xSystem.m_pxDrawManager->Draw(m_pxSwordVertical->GetAnimatedSprite(), m_pxSwordVertical->GetX() - m_xSystem.m_pxDrawManager->GetCameraX(), m_pxSwordVertical->GetY() - m_xSystem.m_pxDrawManager->GetCameraY());

	

}

IState* GameState::NextState()
{
	if (m_pxPlayer->m_iHealth > 0)
	{
		return new WinState(m_xSystem);
	}
	else
	{
		return new LoseState(m_xSystem);
	}
}



void GameState::CheckCollision()
{
	// The overlap variables will be passed as reference in the CollisionManagers Check collision function and in
	// the check function collision and potential overlap will be calculated.
	int iOverlapX = 0;
	int iOverlapY = 0;

	auto it = m_paxTiles.begin();
	while (it != m_paxTiles.end())
	{
		if((*it)->GetType() == WALL)
		{
			if (CollisionManager::Check(m_pxPlayer->GetCollider(), (*it)->GetCollider(), iOverlapX, iOverlapY))
			{
				
				if (abs(iOverlapX ) > abs(iOverlapY))
				{
					m_pxPlayer->SetPosition(m_pxPlayer->GetX() + iOverlapX , m_pxPlayer->GetY());
				}
				else
				{
					m_pxPlayer->SetPosition(m_pxPlayer->GetX(), m_pxPlayer->GetY() + iOverlapY);

				}
			}
			
		}
		it++;
	}
	if (m_pxBoss->IsVisible())
	{
		if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxBoss->GetProjectileCenterCollider(), iOverlapX, iOverlapY) && !m_bPlayerInvincible)
		{
			m_pxPlayer->m_iHealth--;
			m_bPlayerInvincible = true;
			m_iInvinsibleIndexPlayer = 1;
		}
		else if (!CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxBoss->GetProjectileCenterCollider(), iOverlapX, iOverlapY) && m_bPlayerInvincible && m_iInvinsibleIndexPlayer == 1)
		{
			m_bPlayerInvincible = false;
		}

		if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxBoss->GetProjectileTopCollider(), iOverlapX, iOverlapY) && !m_bPlayerInvincible)
		{
			m_pxPlayer->m_iHealth--;
			m_bPlayerInvincible = true;
			m_iInvinsibleIndexPlayer = 2;
		}
		else if (!CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxBoss->GetProjectileTopCollider(), iOverlapX, iOverlapY) && m_bPlayerInvincible && m_iInvinsibleIndexPlayer == 2)
		{
			m_bPlayerInvincible = false;
		}

		if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxBoss->GetProjectileBottomCollider(), iOverlapX, iOverlapY) && !m_bPlayerInvincible)
		{
			m_pxPlayer->m_iHealth--;
			m_bPlayerInvincible = true;
			m_iInvinsibleIndexPlayer = 3;

		}
		else if (!CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxBoss->GetProjectileBottomCollider(), iOverlapX, iOverlapY) && m_bPlayerInvincible && m_iInvinsibleIndexPlayer == 3)
		{
			m_bPlayerInvincible = false;
		}

		if (CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxBoss->GetCollider(), iOverlapX, iOverlapY) && !m_bPlayerInvincible)
		{
			m_pxPlayer->m_iHealth--;
			m_bPlayerInvincible = true;
			m_iInvinsibleIndexPlayer = 4;

		}
		else if (!CollisionManager::Check(m_pxPlayer->GetCollider(), m_pxBoss->GetCollider(), iOverlapX, iOverlapY) && m_bPlayerInvincible && m_iInvinsibleIndexPlayer == 4)
		{
			m_bPlayerInvincible = false;
		}

		if (CollisionManager::Check(m_pxSwordHorizontal->GetCollider(), m_pxBoss->GetCollider(), iOverlapX, iOverlapY) && !m_bBossInvincible)
		{
			m_pxSlash->PlaySound();
			m_pxBoss->m_iHealth--;
			m_bBossInvincible = true;
			m_iInvinsibleIndexBoss = 1;
		}
		else if (!CollisionManager::Check(m_pxSwordHorizontal->GetCollider(), m_pxBoss->GetCollider(), iOverlapX, iOverlapY) && m_bBossInvincible && m_iInvinsibleIndexBoss == 1)
		{
			m_bBossInvincible = false;
		}

		if (CollisionManager::Check(m_pxSwordVertical->GetCollider(), m_pxBoss->GetCollider(), iOverlapX, iOverlapY) && !m_bBossInvincible)
		{
			m_pxSlash->PlaySound();
			m_pxBoss->m_iHealth--;
			m_bBossInvincible = true;
			m_iInvinsibleIndexBoss = 2;
		}
		else if (!CollisionManager::Check(m_pxSwordVertical->GetCollider(), m_pxBoss->GetCollider(), iOverlapX, iOverlapY) && m_bBossInvincible && m_iInvinsibleIndexBoss == 2)
		{
			m_bBossInvincible = false;
		}
	}
	if (CollisionManager::Check(m_pxTriforce->GetCollider(), m_pxPlayer->GetCollider(), iOverlapX, iOverlapY))
	{
		m_pxMusic->StopMusic();
		m_pxFanfare->PlaySound();
		m_bWin = true;
	}

}

#pragma once

#include "IState.h"
class Player;
class HUD;
class HUDElement;
class AnimatedSprite;
class Sprite;
class Tile;
class SwordVertical;
class SwordHorizontal;
class Boss;
class Collider;
class Music;
class Sound;

class GameState : public IState
{
public:
	GameState(System& p_xSystem);
	~GameState();
	void Enter();
	void Exit();
	void UpdateCamera(int p_iDirection);
	bool Update(float p_fDeltaTime);
	void Draw();
	IState* NextState();

private:
	bool m_bPlayerInvincible;
	bool m_bBossInvincible;
	bool m_bWin;
	int m_iInvinsibleIndexPlayer;
	int m_iInvinsibleIndexBoss;
	void CheckCollision();
	Music* m_pxMusic;
	Sound* m_pxSlash;
	Sound* m_pxFanfare;
	HUDElement* m_pxHeart1;
	HUDElement* m_pxHeart2;
	HUDElement* m_pxHeart3;
	HUD* m_pxFloor;
	HUD* m_pxHUD;
	AnimatedSprite* m_pxSpriteProjectile;
	Tile* m_pxTriforce;
	SwordHorizontal* m_pxSwordHorizontal;
	SwordVertical* m_pxSwordVertical;
	Boss* m_pxBoss;
	std::vector<TileData> m_axTileData;
	std::vector<Tile*> m_paxTiles;
	System m_xSystem;
	Player* m_pxPlayer;
	Mix_Chunk* m_xBounceSound;
	Mix_Music* m_xMusic;

};

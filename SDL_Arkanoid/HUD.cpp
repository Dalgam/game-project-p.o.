#include "stdafx.h"
#include "HUD.h"
#include "Player.h"


HUD::HUD(Sprite* p_pxSprite, float p_fX, float p_fY)
{
	m_pxSprite = p_pxSprite;
	m_fX = p_fX;
	m_fY = p_fY;
}

HUD::~HUD()
{

}
void HUD::SetPosition(float p_fX, float p_fY)
{
	m_fX = p_fX;
	m_fY = p_fY;
}

float HUD::GetX()
{
	return m_fX;
}

float HUD::GetY()
{
	return m_fY;
}
void HUD::Update(float p_fDeltaTime)
{

}
Sprite* HUD::GetSprite()
{
	return m_pxSprite;
}
bool HUD::IsVisible() { return m_bVisible; }
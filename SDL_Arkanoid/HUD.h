#pragma once



class HUD
{
public:
	HUD(Sprite* p_pxSprite, float p_fX, float p_fY);
	~HUD();
	void Update(float p_fDeltaTime);
	void SetPosition(float p_fX, float p_f);
	float GetX();
	float GetY();
	Sprite* GetSprite();
	bool IsVisible();

private:
	Sprite* m_pxSprite;
	float m_fX;
	float m_fY;
	bool m_bVisible;


};
#include "stdafx.h"
#include "HUDElement.h"
#include "AnimatedSprite.h"
#include "Player.h"


HUDElement::HUDElement(AnimatedSprite* p_pxAnimatedSprite, int p_iX, int p_iY)
{
	m_pxAnimatedSprite = p_pxAnimatedSprite;
	m_iX = p_iX;
	m_iY = p_iY;
}

HUDElement::~HUDElement()
{

}
void HUDElement::SetPosition(int p_iX, int p_iY)
{
	m_iX = p_iX;
	m_iY = p_iY;
}

int HUDElement::GetX()
{
	return m_iX;
}

int HUDElement::GetY()
{
	return m_iY;
}
void HUDElement::Update(float p_fDeltaTime)
{

}
AnimatedSprite* HUDElement::GetAnimatedSprite()
{
	return m_pxAnimatedSprite;
}
bool HUDElement::IsVisible() { return m_bVisible; }
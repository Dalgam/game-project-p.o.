#pragma once

class AnimatedSprite;

class HUDElement
{
public:
	HUDElement(AnimatedSprite* p_pxAnimatedSprite, int p_iX, int p_iY);
	~HUDElement();
	void Update(float p_fDeltaTime);
	void SetPosition(int p_iX, int p_Y);
	int GetX();
	int GetY();
	AnimatedSprite* GetAnimatedSprite();
	bool IsVisible();

private:
	AnimatedSprite* m_pxAnimatedSprite;
	int m_iX;
	int m_iY;
	bool m_bVisible;


};
#pragma once

// If we want to make more entity types we can keep adding it to this list, this is to distinguish what they really are if we
// store different class* that inherit from IEntity in an IEntity* vector for example.
enum EENTITYTYPE
{
	ENTITY_PLAYER,
	ENTITY_BOSS,
	ENTITY_SWORD,
	ENTITY_PROJECTILE
};

class Sprite;
class Collider;
class AnimatedSprite;

class IEntity
{
public:
	~IEntity() {};
	//virtual void Update(float p_fDeltaTime, int p_iCameraX, int p_iCameraY) = 0;
	virtual Collider* GetCollider() = 0;
	virtual float GetX() = 0;
	virtual float GetY() = 0;
	virtual bool IsVisible() = 0;
};
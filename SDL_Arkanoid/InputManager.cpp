#include "stdafx.h"
#include "InputManager.h"

InputManager::InputManager()
{

	for (int i = 0; i < 256; i++)
	{
		m_abKeys[i] = false;
	}
	for (int i = 0; i < 3; i++)
	{
		m_abButtons[i] = false;
	}
	m_iX = 0;
	m_iY = 0;

}

InputManager::~InputManager()
{

}

void InputManager::Initialize()
{

}

void InputManager::Shutdown()
{

}

int InputManager::GetMouseX(){ return m_iX; }

int InputManager::GetMouseY() { return m_iY; }

void InputManager::SetMousePosition(int p_iX, int p_iY)
{
	m_iX = p_iX;
	m_iY = p_iY;
}

bool InputManager::IsMouseButtonDown(int p_iIndex) 
{
	if (p_iIndex < 0)
		return false;
	if (p_iIndex > 2)
		return false;

	return m_abButtons[p_iIndex];
}

void InputManager::SetMouseButton(int p_iIndex, bool p_bValue)
{
	if (p_iIndex < 0)
		return;
	if (p_iIndex > 2)
		return;

	m_abButtons[p_iIndex] = p_bValue;
}

bool InputManager::IsKeyDown(int p_iIndex)
{
	if (p_iIndex < 0)
		return false;
	if (p_iIndex > 255)
		return false;

	return m_abKeys[p_iIndex];
}



void InputManager::SetKey(int p_iIndex,
	bool p_bValue)
{
	if (p_iIndex < 0)
		return;
	if (p_iIndex > 255)
		return;

	m_abKeys[p_iIndex] = p_bValue;
}

void InputManager::Update()
{
	SDL_Event xEvent;
	while (SDL_PollEvent(&xEvent))
	{
		if (xEvent.type == SDL_MOUSEMOTION)
		{
			SetMousePosition(xEvent.motion.x, xEvent.motion.y);
		}
		else if (xEvent.type == SDL_MOUSEBUTTONDOWN)
		{
			SetMouseButton(xEvent.button.button,true);
		}
		else if (xEvent.type == SDL_MOUSEBUTTONUP)
		{
			SetMouseButton(xEvent.button.button, false);
		}
		else if (xEvent.type == SDL_KEYDOWN)
		{
			SetKey(xEvent.key.keysym.sym, true);
		}
		else if (xEvent.type == SDL_KEYUP)
		{
			SetKey(xEvent.key.keysym.sym, false);
		}
	}
}
#pragma once

	/*
	Suggested structure for a potential future InputManager
	Init will create objects needed and shutdown will delete them.
	Update will handle all the events with SDL_PollEvent update the 
	mouse and keyboard objects accordingly.
	Other functions will return appropriate data from the mouse and keyboard.
	*/


class InputManager
{
public:
	InputManager();
	~InputManager();
	void Initialize();
	void Shutdown();
	int GetMouseX();
	int GetMouseY();
	void SetMousePosition(int p_iX, int p_iY);
	bool IsMouseButtonDown(int p_iIndex);
	void SetMouseButton(int p_iIndex, bool p_bValue);
	bool IsKeyDown(int p_iIndex) ;
	void SetKey(int p_iIndex, bool p_bValue);
	void Update();
private:
	SDL_Event xEvent;

	bool m_abKeys[256];
	bool m_abButtons[3];
	int m_iX;
	int m_iY;
};

#pragma once


#include "IState.h"
class HUD;
class HUDElement;
class Sprite;
class InputManager;

class LoseState : public IState
{
public:
	LoseState(System& p_xSystem);
	~LoseState();
	void Enter();
	void Exit();
	bool Update(float p_fDeltaTime);
	void Draw();
	IState* NextState();
private:
	HUD* m_pxTitleScreen;
	System m_xSystem;


};

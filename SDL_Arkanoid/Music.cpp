#include "stdafx.h"
#include "Music.h"

Music::Music(Mix_Music* p_pxMusic)
{
	m_pxMusic = p_pxMusic;
}
Music::~Music()
{

}
void Music::PlayMusic(int p_iLoop)
{
	Mix_PlayMusic(m_pxMusic, p_iLoop);
}
void Music::StopMusic()
{
	Mix_HaltMusic();
}
void Music::PauseMusic()
{
	Mix_ResumeMusic();
}
void Music::ResumeMusic()
{
	Mix_PauseMusic();
}
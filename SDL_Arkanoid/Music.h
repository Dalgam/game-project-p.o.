#pragma once


class Music
{
public:
	Music(Mix_Music* p_pxMusic);
	~Music();
	void PlayMusic(int p_iLoop);
	void StopMusic();
	void PauseMusic();
	void ResumeMusic();
private:
	Mix_Music* m_pxMusic;
};
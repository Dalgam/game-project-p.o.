#include "stdafx.h"
#include "Player.h"
#include "InputManager.h"
#include "AnimatedSprite.h"
#include "Collider.h"
#include "SwordVertical.h"
#include "SwordHorizontal.h"

Player::Player(InputManager* p_pxInputManager, AnimatedSprite* p_pxAnimatedSprite, SwordHorizontal* p_pxSwordHorizontal, SwordVertical* p_pxSwordVertical,
	float p_fX, float p_fY, int p_iCameraWidth, int p_iCameraHeight, int p_iLevelWidth, int p_iLevelHeight)
{
	m_pxInputManager = p_pxInputManager;
	m_pxAnimatedSprite = p_pxAnimatedSprite;
	
	m_fX = p_fX;
	m_fY = p_fY;
	m_iCameraWidth = p_iCameraWidth;
	m_iCameraHeight = p_iCameraHeight;
	m_iLevelWidth = p_iLevelWidth;
	m_iLevelHeight = p_iLevelHeight;
	m_fTimer = 0.0;
	m_pxSwordHorizontal = p_pxSwordHorizontal;
	m_pxSwordVertical = p_pxSwordVertical;
	m_iCurrentDirection = DOWN;
	m_pxCollider = new Collider(32, 32);
	m_pxCollider->SetParent(this);
	
	m_bVisible = true;
}
Player::~Player()
{
	delete m_pxCollider;
	m_pxCollider = nullptr;
}
void Player::Update(float p_fDeltaTime, int p_iCameraX, int p_iCameraY)
{

	//TODO:add attacks and stuff
	if (m_pxInputManager->IsKeyDown(SDLK_w))
	{
		m_pxAnimatedSprite->Update(p_fDeltaTime, 4, 5);
		m_fY -= 4;
		m_iCurrentDirection = UP;
		
	}
	else if (m_pxInputManager->IsKeyDown(SDLK_s))
	{
		m_pxAnimatedSprite->Update(p_fDeltaTime, 0, 1);
		m_fY += 4;
		m_iCurrentDirection = DOWN;
		
		
	}

	else if (m_pxInputManager->IsKeyDown(SDLK_d))
	{
		m_pxAnimatedSprite->Update(p_fDeltaTime, 6, 7);
		m_fX += 4;
		m_iCurrentDirection = RIGHT;
	
	}
	else if (m_pxInputManager->IsKeyDown(SDLK_a))
	{
		m_pxAnimatedSprite->Update(p_fDeltaTime, 2, 3);
		m_fX -= 4;
		m_iCurrentDirection = LEFT;

	}
	else
	{
		m_pxAnimatedSprite->Update(p_fDeltaTime, m_iCurrentDirection, m_iCurrentDirection);
	}
	m_fTimer += p_fDeltaTime;
	if (m_pxInputManager->IsKeyDown(SDLK_SPACE) && m_fCooldown <= m_fTimer)
	{
		if (m_iCurrentDirection == UP)
		{
			m_pxSwordHorizontal->SetPosition(0, 0);
			m_pxSwordVertical->GetAnimatedSprite()->Update(p_fDeltaTime, 1, 1);
			m_pxSwordVertical->SetPosition(m_fX + (m_pxSwordVertical->GetAnimatedSprite()->GetRegion()->w/2), m_fY - m_pxSwordVertical->GetAnimatedSprite()->GetRegion()->h);

		}
		if (m_iCurrentDirection == DOWN)
		{
			m_pxSwordHorizontal->SetPosition(0, 0);
			m_pxSwordVertical->GetAnimatedSprite()->Update(p_fDeltaTime, 0, 0);
			m_pxSwordVertical->SetPosition(m_fX + (m_pxSwordVertical->GetAnimatedSprite()->GetRegion()->w / 2), m_fY  + m_pxAnimatedSprite->GetRegion()->h - 2);
		}
		if (m_iCurrentDirection == LEFT)
		{
			m_pxSwordVertical->SetPosition(0, 0);
			m_pxSwordHorizontal->GetAnimatedSprite()->Update(p_fDeltaTime, 0, 0);
			m_pxSwordHorizontal->SetPosition(m_fX - (m_pxSwordHorizontal->GetAnimatedSprite()->GetRegion()->w), m_fY + m_pxSwordHorizontal->GetAnimatedSprite()->GetRegion()->h/2);
		}
		if (m_iCurrentDirection == RIGHT)
		{
			m_pxSwordVertical->SetPosition(0, 0);
			m_pxSwordHorizontal->GetAnimatedSprite()->Update(p_fDeltaTime, 1, 1);
			m_pxSwordHorizontal->SetPosition(m_fX + (m_pxAnimatedSprite->GetRegion()->w) - 2, m_fY + m_pxAnimatedSprite->GetRegion()->h/2 - m_pxSwordHorizontal->GetAnimatedSprite()->GetRegion()->h/2);
		}

		m_fTimer = 0.0f;
	}
	else if(m_fCooldown <= m_fTimer)
	{
		m_pxSwordVertical->SetPosition(0, 0);
		m_pxSwordHorizontal->SetPosition(0, 0);
		m_pxAnimatedSprite->Update(p_fDeltaTime, m_iCurrentDirection, m_iCurrentDirection);
	}

	if (m_fX < p_iCameraX)
	{
		m_fX = p_iCameraX;
	}
	if ((m_fX + m_pxAnimatedSprite->GetRegion()->w) > (p_iCameraX + m_iCameraWidth))
	{
		m_fX = (p_iCameraX + m_iCameraWidth) - m_pxAnimatedSprite->GetRegion()->w;
	}
	if (m_fY < (p_iCameraY + 160))
	{
		m_fY = (p_iCameraY + 160);
	}
	if ((m_fY + m_pxAnimatedSprite->GetRegion()->h) > (p_iCameraY + m_iCameraHeight))
	{
		m_fY = (p_iCameraY + m_iCameraHeight) - m_pxAnimatedSprite->GetRegion()->h;
	}
	
	m_pxCollider->Refresh();
}

void Player::SetPosition(float p_fX, float p_fY)
{
	m_fX = p_fX;
	m_fY = p_fY;
}

AnimatedSprite* Player::GetAnimatedSprite() { return m_pxAnimatedSprite; }

Collider* Player::GetCollider(){ return m_pxCollider; }

float Player::GetX() { return m_fX; }

float Player::GetY() { return m_fY; }

bool Player::IsVisible() { return m_fX; }

EENTITYTYPE Player::GetType() { return EENTITYTYPE::ENTITY_PLAYER; }
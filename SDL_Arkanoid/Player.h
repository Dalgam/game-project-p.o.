#pragma once

#include "IEntity.h"
enum PLAYER_SPRITE_DIRECTION
{
	UP = 4,
	DOWN = 0,
	LEFT= 2,
	RIGHT=6
};



class AnimatedSprite;
class InputManager;
class SwordHorizontal;
class SwordVertical;


class Player : public IEntity
{
public:
	Player(InputManager* p_pxInputManager, AnimatedSprite* p_pxAnimatedSprite, SwordHorizontal* p_pxSwordHorizontal, SwordVertical* p_pxSwordVertical, float p_fX, float p_fY, int p_iCameraWidth, int p_iCameraHeightint, int p_iLevelWidth, int p_iLevelHeight);
	~Player();
	void Update(float p_fDeltaTime, int p_iCameraX, int p_iCameraY);
	AnimatedSprite* GetAnimatedSprite();
	Collider* GetCollider();
	float GetX();
	float GetY();
	int m_iHealth = 6;
	bool IsVisible();
	EENTITYTYPE GetType();
	void SetPosition(float p_fX, float p_fY);


private:
	Player() {};
	AnimatedSprite* m_pxAnimatedSprite;
	Collider* m_pxCollider;
	SwordHorizontal* m_pxSwordHorizontal;
	SwordVertical* m_pxSwordVertical;
	InputManager* m_pxInputManager;
	float m_fX;
	float m_fY;
	float m_fCooldown = 0.001f;
	float m_fTimer;
	int m_iCurrentDirection;
	int m_iLevelWidth;
	int m_iLevelHeight;
	int m_iCameraWidth;
	int m_iCameraHeight;
	bool m_bVisible;


};
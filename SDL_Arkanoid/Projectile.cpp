#include "stdafx.h"
#include "Projectile.h"
#include "InputManager.h"
#include "AnimatedSprite.h"
#include "Collider.h"


Projectile::Projectile(AnimatedSprite* p_pxAnimatedSprite,
	float p_fX, float p_fY,float p_fDirX,float p_fDirY, int p_iCameraWidth, int p_iCameraHeight)
{
	m_fDirX = p_fDirX;
	m_fDirY = p_fDirY;
	m_fX = p_fX;
	m_fY = p_fY;
	m_iCameraWidth = p_iCameraWidth;
	m_iCameraHeight = p_iCameraHeight;
	m_pxCollider = new Collider(14, 24);
	m_fSpeed = 250.0f;
	m_pxCollider->SetParent(this);
	m_bVisible = true;
}
Projectile::~Projectile()
{
	delete m_pxCollider;
	m_pxCollider = nullptr;

}
void Projectile::Update(float p_fDeltaTime, int p_iCameraX, int p_iCameraY)
{

	m_fX += m_fDirX * m_fSpeed * p_fDeltaTime;
	m_fY += m_fDirY * m_fSpeed * p_fDeltaTime;

	m_pxCollider->Refresh();
}

void Projectile::SetPosition(float p_fX, float p_fY,float p_fDirX, float p_fDirY)
{
	m_fDirX = p_fDirX;
	m_fDirY = p_fDirY;
	m_fX = p_fX;
	m_fY = p_fY;
}

AnimatedSprite* Projectile::GetAnimatedSprite() { return m_pxAnimatedSprite; }

Collider* Projectile::GetCollider() { return m_pxCollider; }

float Projectile::GetX() { return m_fX; }

float Projectile::GetY() { return m_fY; }

bool Projectile::IsVisible() { return m_bVisible; }

EENTITYTYPE Projectile::GetType() { return EENTITYTYPE::ENTITY_PROJECTILE; }
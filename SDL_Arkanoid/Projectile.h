#pragma once



#include "IEntity.h"

class AnimatedSprite;
class InputManager;

class Projectile : public IEntity
{
public:
	Projectile(AnimatedSprite* p_pxAnimatedSprite, float p_fX, float p_fY, float p_fDirX, float p_fDirY, int p_iCameraWidth, int p_iCameraHeightint);
	~Projectile();
	void Update(float p_fDeltaTime, int p_iCameraX, int p_iCameraY);
	AnimatedSprite* GetAnimatedSprite();
	Collider* GetCollider();

	float GetX();
	float GetY();
	bool IsVisible();
	EENTITYTYPE GetType();
	void SetPosition(float p_fX, float p_fY, float p_fDirX, float p_fDirY);


private:
	Projectile() {};
	AnimatedSprite* m_pxAnimatedSprite;
	Collider* m_pxCollider;

	InputManager* m_pxInputManager;
	float m_fX;
	float m_fY;
	float m_fDirX;
	float m_fDirY;
	float m_fSpeed;
	int m_iCameraWidth;
	int m_iCameraHeight;
	bool m_bVisible;


};
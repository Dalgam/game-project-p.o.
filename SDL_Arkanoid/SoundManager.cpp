#include "stdafx.h"
#include "SoundManager.h"
#include "Sound.h"
#include "Music.h"




SoundManager::SoundManager()
{

}


SoundManager::~SoundManager()
{



}

bool SoundManager::Initialize()
{
	Mix_Init(MIX_INIT_MP3);
	if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 1024) == -1)
	{
		const char* error = Mix_GetError();
		SDL_Log(error);
		return false;
	}
	return true;
}

void SoundManager::Shutdown()
{
	auto it = m_apxSounds.begin();
	while (it != m_apxSounds.end())
	{
		delete (*it);
		it++;
	}
	m_apxSounds.clear();

	auto it2 = m_apxMusic.begin();
	while (it2 != m_apxMusic.end())
	{
		delete (*it2);
		it2++;
	}
	m_apxMusic.clear();
	Mix_CloseAudio();
	Mix_Quit();
}

Sound* SoundManager::CreateSound(const std::string& p_sFilepath)
{
	auto it = m_apxAudio.find(p_sFilepath);
	if (it == m_apxAudio.end())
	{
		Mix_Chunk* xAudio = Mix_LoadWAV(p_sFilepath.c_str());
		m_apxAudio.insert(std::pair<std::string, Mix_Chunk*>(p_sFilepath, xAudio));
		it = m_apxAudio.find(p_sFilepath);
	}
	Sound* xSound = new Sound((it->second));
	m_apxSounds.push_back(xSound);
	return xSound;
}

void SoundManager::DestroySound(Sound* p_pxSound)
{
	auto it = m_apxSounds.begin();
	while (it != m_apxSounds.end())
	{
		if ((*it) == p_pxSound)
		{
			delete (*it);
			m_apxSounds.erase(it);
			return;
		}
		it++;
	}
}

Music* SoundManager::CreateMusic(const std::string & p_sFilepath)
{
	auto it = m_apxSong.find(p_sFilepath);
	if (it == m_apxSong.end())
	{
		Mix_Music* xSong = Mix_LoadMUS(p_sFilepath.c_str());
		m_apxSong.insert(std::pair<std::string, Mix_Music*>(p_sFilepath, xSong));
		it = m_apxSong.find(p_sFilepath);
	}
	Music* xMusic = new Music((it->second));
	m_apxMusic.push_back(xMusic);
	return xMusic;
}

void SoundManager::DestroyMusic(Music * p_pxMusic)
{
	auto it = m_apxMusic.begin();
	while (it != m_apxMusic.end())
	{
		if ((*it) == p_pxMusic)
		{
			delete (*it);
			m_apxMusic.erase(it);
			return;
		}
		it++;
	}
}



#pragma once

class Sound;
class Music;

class SoundManager
{
public:
	SoundManager();
	~SoundManager();

	bool Initialize();
	void Shutdown();

	Sound* CreateSound(const std::string& p_sFilepath);
	void DestroySound(Sound* p_pxSound);

	Music* CreateMusic(const std::string& p_sFilepath);
	void DestroyMusic(Music* p_pxMusic);
private:
	std::vector<Sound*> m_apxSounds;
	std::map<std::string, Mix_Chunk*> m_apxAudio;
	std::vector<Music*> m_apxMusic;
	std::map<std::string, Mix_Music*> m_apxSong;
};
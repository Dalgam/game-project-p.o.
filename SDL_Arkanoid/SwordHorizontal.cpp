#include "stdafx.h"
#include "SwordHorizontal.h"
#include "InputManager.h"
#include "AnimatedSprite.h"
#include "Collider.h"


SwordHorizontal::SwordHorizontal(InputManager* p_pxInputManager, AnimatedSprite* p_pxAnimatedSprite,
	float p_fX, float p_fY, int p_iCameraWidth, int p_iCameraHeight, int p_iLevelWidth, int p_iLevelHeight)
{
	m_pxInputManager = p_pxInputManager;
	m_pxAnimatedSprite = p_pxAnimatedSprite;
	m_fX = p_fX;
	m_fY = p_fY;
	m_iCameraWidth = p_iCameraWidth;
	m_iCameraHeight = p_iCameraHeight;
	m_iLevelWidth = p_iLevelWidth;
	m_iLevelHeight = p_iLevelHeight;
	m_pxCollider = new Collider(24, 14);
	m_pxCollider->SetParent(this);
	m_bVisible = true;
}
SwordHorizontal::~SwordHorizontal()
{
	delete m_pxCollider;
	m_pxCollider = nullptr;

}
void SwordHorizontal::Update(float p_fDeltaTime, int p_iCameraX, int p_iCameraY)
{


	m_pxCollider->Refresh();
}

void SwordHorizontal::SetPosition(float p_fX, float p_fY)
{
	m_fX = p_fX;
	m_fY = p_fY;
}

AnimatedSprite* SwordHorizontal::GetAnimatedSprite() { return m_pxAnimatedSprite; }

Collider* SwordHorizontal::GetCollider() { return m_pxCollider; }

float SwordHorizontal::GetX() { return m_fX; }

float SwordHorizontal::GetY() { return m_fY; }

bool SwordHorizontal::IsVisible() { return m_fX; }

EENTITYTYPE SwordHorizontal::GetType() { return EENTITYTYPE::ENTITY_SWORD; }
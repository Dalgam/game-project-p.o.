#pragma once



#include "IEntity.h"

class AnimatedSprite;
class InputManager;

class SwordHorizontal : public IEntity
{
public:
	SwordHorizontal(InputManager* p_pxInputManager, AnimatedSprite* p_pxAnimatedSprite, float p_fX, float p_fY, int p_iCameraWidth, int p_iCameraHeightint, int p_iLevelWidth, int p_iLevelHeight);
	~SwordHorizontal();
	void Update(float p_fDeltaTime, int p_iCameraX, int p_iCameraY);
	AnimatedSprite* GetAnimatedSprite();
	Collider* GetCollider();

	float GetX();
	float GetY();
	bool IsVisible();
	EENTITYTYPE GetType();
	void SetPosition(float p_fX, float p_fY);


private:
	SwordHorizontal() {};
	AnimatedSprite* m_pxAnimatedSprite;
	Collider* m_pxCollider;

	InputManager* m_pxInputManager;
	float m_fX;
	float m_fY;
	int m_iLevelWidth;
	int m_iLevelHeight;
	int m_iCameraWidth;
	int m_iCameraHeight;
	bool m_bVisible;


};
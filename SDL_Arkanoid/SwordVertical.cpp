#include "stdafx.h"
#include "SwordVertical.h"
#include "InputManager.h"
#include "AnimatedSprite.h"
#include "Collider.h"


SwordVertical::SwordVertical(InputManager* p_pxInputManager, AnimatedSprite* p_pxAnimatedSprite,
	float p_fX, float p_fY, int p_iCameraWidth, int p_iCameraHeight, int p_iLevelWidth, int p_iLevelHeight)
{
	m_pxInputManager = p_pxInputManager;
	m_pxAnimatedSprite = p_pxAnimatedSprite;
	m_fX = p_fX;
	m_fY = p_fY;
	m_iCameraWidth = p_iCameraWidth;
	m_iCameraHeight = p_iCameraHeight;
	m_iLevelWidth = p_iLevelWidth;
	m_iLevelHeight = p_iLevelHeight;
	m_pxCollider = new Collider(14, 24);
	m_pxCollider->SetParent(this);
	m_bVisible = true;
}
SwordVertical::~SwordVertical()
{
	delete m_pxCollider;
	m_pxCollider= nullptr;
	
}
void SwordVertical::Update(float p_fDeltaTime, int p_iCameraX, int p_iCameraY)
{


	m_pxCollider->Refresh();
}

void SwordVertical::SetPosition(float p_fX, float p_fY)
{
	m_fX = p_fX;
	m_fY = p_fY;
}

AnimatedSprite* SwordVertical::GetAnimatedSprite() { return m_pxAnimatedSprite; }

Collider* SwordVertical::GetCollider() { return m_pxCollider; }

float SwordVertical::GetX() { return m_fX; }

float SwordVertical::GetY() { return m_fY; }

bool SwordVertical::IsVisible() { return m_fX; }

EENTITYTYPE SwordVertical::GetType() { return EENTITYTYPE::ENTITY_SWORD; }
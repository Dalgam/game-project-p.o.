#include "stdafx.h"
#include "Tile.h"
#include "Sprite.h"
#include "Collider.h"


Tile::Tile(TileData p_xTileData, int p_iX, int p_iY)
{
	m_iX = p_iX;
	m_iY = p_iY;
	m_pxSprite = p_xTileData.TileSprite;
	m_iTileType = p_xTileData.TileType;
	m_pxCollider = new Collider(m_pxSprite->GetRegion()->w, m_pxSprite->GetRegion()->h);
	m_pxCollider->SetParent(this);
	m_pxCollider->SetPosition(m_iX, m_iY);
	m_bVisible = true;
}
Tile::~Tile()
{
	
}

void Tile::Update(float p_fDeltaTime, int p_iCameraX, int p_iCameraY)
{


}


Sprite* Tile::GetSprite()
{
	return m_pxSprite;
}
float Tile::GetX()
{
	return m_iX;
}
float Tile::GetY()
{
	return m_iY;
}
bool Tile::IsVisible()
{
	return m_bVisible;
}
int Tile::GetType()
{
	return m_iTileType;
}

Collider* Tile::GetCollider()
{
	return m_pxCollider;
}
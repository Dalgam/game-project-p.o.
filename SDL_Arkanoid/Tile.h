#pragma once
#include "IEntity.h"
class Sprite;
enum TILETYPE
{
	WALL=1,
	FLOOR=2
};


class Tile : public IEntity
{
public:
	Tile(TileData p_xTileData, int p_iX, int p_iY);
	~Tile();

	Sprite* GetSprite();
	void Update(float p_fDeltaTime, int p_iCameraX, int p_iCameraY);
	
	float GetX();
	float GetY();
	bool IsVisible();
	int GetType();
	Collider* GetCollider();
private:
	Collider* m_pxCollider;
	Sprite* m_pxSprite;
	int m_iX;
	int m_iY;
	int m_iTileType;
	bool m_bVisible;

};
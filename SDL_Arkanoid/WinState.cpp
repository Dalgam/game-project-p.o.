#include "stdafx.h"
#include <fstream>
#include <string>
#include "WinState.h"
#include "SpriteManager.h"
#include "DrawManager.h"
#include "Sprite.h"
#include "HUD.h"
#include "HUDElement.h"
#include "AnimatedSprite.h"
#include "InputManager.h"
#include "GameState.h"


WinState::WinState(System& p_xSystem)
{

	m_xSystem = p_xSystem;



}

WinState::~WinState()
{

}

void WinState::Enter()
{



	m_xSystem.m_pxDrawManager->SetCameraX(1536);
	m_xSystem.m_pxDrawManager->SetCameraY(544);




	Sprite* xHUD = m_xSystem.m_pxSpriteManager->CreateSprite("../assets/YouWin.png", 0, 0, 512, 512);
	m_pxTitleScreen = new HUD(xHUD, m_xSystem.m_pxDrawManager->GetCameraX(), m_xSystem.m_pxDrawManager->GetCameraY());



}

void WinState::Exit()
{
	m_xSystem.m_pxSpriteManager->DestroySprite(m_pxTitleScreen->GetSprite());
	delete m_pxTitleScreen;
	m_pxTitleScreen = nullptr;
}

bool WinState::Update(float p_fDeltaTime)
{
	if (m_xSystem.m_pxInputManager->IsKeyDown(SDLK_RETURN))
	{

		return false;
	}

	return true;
}



void WinState::Draw()
{

	m_xSystem.m_pxDrawManager->Draw(m_pxTitleScreen->GetSprite(), m_pxTitleScreen->GetX() - m_xSystem.m_pxDrawManager->GetCameraX(), m_pxTitleScreen->GetY() - m_xSystem.m_pxDrawManager->GetCameraY());


}

IState* WinState::NextState()
{
	return new GameState(m_xSystem);
}

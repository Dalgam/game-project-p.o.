#pragma once


#include "IState.h"
class HUD;
class HUDElement;
class Sprite;
class InputManager;

class WinState : public IState
{
public:
	WinState(System& p_xSystem);
	~WinState();
	void Enter();
	void Exit();
	bool Update(float p_fDeltaTime);
	void Draw();
	IState* NextState();
private:
	HUD* m_pxTitleScreen;
	System m_xSystem;


};
